import React, { useCallback, useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight, MdShoppingCart } from "react-icons/md";
import { FaStar } from 'react-icons/fa';
import { FaRegStar } from "react-icons/fa6";
import './Detail.css';
import './Grid.css';
import './ProductItem.css';
import { Product } from './Slider';
import ProductItem from './ProductItem';
import Review, { File } from './Review';
import { OrderDetail } from './Products';
import { Link, redirect, useNavigate } from 'react-router-dom';
import Image from './Image';

export interface Review {
  id: number;
  product: number;
  customer: string;
  reviewText: string;
  rate: number;
  createDate: Date;
  photos: File[]

}
export interface RatingProps {
  value: number;
}

export interface Stock {
  value: number;
}

export interface CreateDate {
  createDate: Date;
}

export function Rating(props: RatingProps) {
  const STAR_COUNT = 5;
  const { value } = props;

  const stars = Array.from({ length: STAR_COUNT }, (_, i) => {
    if (i < value) {
      return (<FaStar key={i} />)
    }

    return (<FaRegStar key={i} />)
  });

  return <div className="rating">{stars}</div>;
}

export function DisplayCreateDate(props: CreateDate) {
  const { createDate } = props;
  const formatDate = new Date(createDate).toLocaleDateString('en-GB', { timeZone: 'UTC' })

  return (
    <div className='detail-review-item_createDate'>
      {formatDate}
    </div>
  )
}

export function DisplayReview(review: Review) {

  return (
    <div className="detail-review-item">
      <div className='detail-review-item_user'>
        <div>{review.customer}</div>
        <Rating value={review.rate} />
        <DisplayCreateDate createDate={review.createDate} />
      </div>
      <div className='detail-review-item_text'>
        {review.reviewText}
      </div>
      <div className='detail-review-item_img'>
        {review.photos?.map((item) => (
          <Image key={item.id} src={item.url} />
        ))}
      </div>
    </div>
  )
}

function Detail() {
  const navigate = useNavigate();

  const [productList, setProductList] = useState<Product[]>([]);
  const [reviewList, setReviewList] = useState<Review[]>([]);
  const [productItem, setProductItem] = useState<Product>();
  const [pageNum, setPageNum] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [averageRate, setAverageRate] = useState(0);
  const [allRate, setAllRate] = useState(0);
  const [fiveRate, setFiveRate] = useState(0);
  const [twoRate, setTwoRate] = useState(0);
  const [threeRate, setThreeRate] = useState(0);
  const [fourRate, setFourRate] = useState(0);
  const [oneRate, setOneRate] = useState(0);
  const [comment, setComment] = useState(0);
  const [rate, setRate] = useState('');
  const [haveComment, setHaveComment] = useState('');
  const [product, setProduct] = useState(0);
  const [styleAllBtn, setStyleAllBtn] = useState('btnAll-active');
  const [styleFiveBtn, setStyleFiveBtn] = useState('');
  const [styleFourBtn, setStyleFourBtn] = useState('');
  const [styleThreeBtn, setStyleThreeBtn] = useState('');
  const [styleTwoBtn, setStyleTwoBtn] = useState('');
  const [styleOneBtn, setStyleOneBtn] = useState('');
  const [styleCommentBtn, setStyleCommentBtn] = useState('');
  const [mainImage, setMainImage] = useState<string>('');
  const [quantity, setQuantity] = useState(1);

  const searchParams = new URLSearchParams(window.location.search);
  const idProduct = Number(searchParams.get('id'));

  // Hàm hiển thị mini image
  const DisplayMiniImage = useCallback((props: File) => {
    const { url } = props;
    return (
      <div className="l-3 m-3 c-3 detail-picture-item">
        <Image onClick={() => setMainImage(url)} src={url} />
      </div>
    )
  }, [])

  // Gọi API lấy thông tin sản phẩm theo productID
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    fetch(`${process.env.REACT_APP_API_URL}/product/${idProduct}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setMainImage(actualData.photos[0]?.url);
        setProduct(actualData.id);
        setProductItem(actualData);
      })
      .catch((err) => {
        setProductItem(undefined);
      })
  }, []);

  // Hàm xử lý hiển thị sold out
  function StatusMessage(props: Stock) {
    const { value } = props;
    if (value > 0) {
      return (
        <div id="detail-rated-star-wrap" className='detail-rated-star'>
          Available
        </div>
      )
    } else {
      return (
        <div id="detail-rated-star-wrap" className='detail-rated-star'>
          Sold out
        </div>
      )
    }
  }

  // Gọi API lấy tất cả review theo productID
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/review/review-list?product=${idProduct}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setOneRate(0);
        setTwoRate(0);
        setThreeRate(0);
        setFourRate(0);
        setFiveRate(0);
        setComment(0);
        const initialValue = 0;

        const sumWithInitial = (actualData.length !== 0 ? actualData.reduce(
          (accumulator: number, currentValue: Review) => accumulator + currentValue.rate,
          initialValue,
        ) : 0);
        setAverageRate(sumWithInitial !== 0 ? (Number((sumWithInitial / actualData.length).toFixed(1))) : 0);
        setAllRate(actualData.length)

        let countOneRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 1) {
            countOneRate += 1
          }
        }
        setOneRate(countOneRate)

        let countTwoRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 2) {
            countTwoRate += 1;
          }
        }
        setTwoRate(countTwoRate);

        let countThreeRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 3) {
            countThreeRate += 1;
          }
        }
        setThreeRate(countThreeRate);

        let countFourRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 4) {
            countFourRate += 1;
          }
        }
        setFourRate(countFourRate);

        let countFiveRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 5) {
            countFiveRate += 1;
          }
        }
        setFiveRate(countFiveRate);

        let countHaveComment = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].reviewText !== '') {
            countHaveComment += 1;
          }
        }
        setComment(countHaveComment);
      })
      .catch((err) => {
        setProductItem(undefined);
      })
  }, []);

  // Gọi API lấy productList
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/`)
      .then((response) => response.json())
      .then((data) => setProductList(data))
      .catch((error) => { });
  }, []);

  //Gọi API lấy review theo filter
  useEffect(() => {
    const searchParams = new URLSearchParams();
    searchParams.append('page', String(pageNum));
    searchParams.append('size', String(2));

    if (rate !== '') {
      searchParams.append('rate', String(rate));
    }

    if (haveComment != '') {
      searchParams.delete('rate');
      searchParams.append('haveComment', String(haveComment));
    }

    searchParams.append('product', String(idProduct));

    fetch(`${process.env.REACT_APP_API_URL}/review/paging?${searchParams}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setReviewList(actualData.content)
        setTotalPages(actualData.totalPages);
      })
  }, [pageNum, rate, haveComment, idProduct]);

  const [style, setStyle] = useState("half-height");

  // Hàm thay đổi kích thước description picture
  const changeStyle = () => {
    if (style !== "half-height") {
      setStyle("half-height");

    } else { setStyle(""); }
  };

  // Hàm xử lý nút previous
  function PreviousButton() {
    return (
      <button disabled={pageNum === 0 || reviewList.length === 0} onClick={() => setPageNum(pageNum - 1)}>&laquo;</button>
    )
  }

  // Hàm xử lý nút next
  function NextButton() {
    return (
      <button disabled={pageNum === totalPages - 1 || reviewList.length === 0} onClick={() => setPageNum(pageNum + 1)}>&raquo;</button>
    )
  }

  //Hàm lưu orderDetail vào localStorage
  function saveLocalStorage() {
    const token = localStorage.getItem('token');

    const orderList: OrderDetail[] = JSON.parse(localStorage.getItem('orders') || '[]');

    if (token) {
      const index = orderList.findIndex(orderDetail =>
        orderDetail.product.id === productItem?.id
      );

      if (index === -1 && productItem) {
        const orderDetail: OrderDetail = {
          product: productItem,
          quantity: quantity,
          sellPrice: productItem?.sellPrice,
          reviewStatus: false,
        }

        orderList.push(orderDetail);
      } else {
        orderList[index].quantity += 1;
      }

      localStorage.setItem("orders", JSON.stringify(orderList));
      window.dispatchEvent(new Event("storage"));
    } else {
      navigate("/login")
    }
  }

  // Hàm hiển thị số trang
  function NumberPagination() {
    let arr = []
    for (let index = 0; index < totalPages; index++) {
      arr.push(
        <button key={index} className={pageNum === index ? 'pagination_active' : ''} onClick={() => {
          setPageNum(index);
        }}>
          {index + 1}
        </button>)
    }
    return (<>{arr}</>);
  }

  return (
    <>
      <div className="detail__nav">
        <div className="row detail__nav-wrap">
          <Link className="detail_nav-home" to="/">
            <b>Home</b>
          </Link>
          <div className="detail-nav-icon">
            <MdKeyboardDoubleArrowRight />
          </div>

          <Link to="/products" className="detail_nav-all-product">
            <b>All Products</b>
          </Link>

          <div className="detail_detail-nav-icon">
            <MdKeyboardDoubleArrowRight />
          </div>
          <div className="detail_nav-all-productName">
            <b>{productItem?.productName}</b>
          </div>
        </div>
      </div>

      <div className='detail__container'>
        <div className="grid wide">
          <div className="row detail__content">
            <div className='l-12 m-12 c-12'>
              <div className="detail__info">
                <div className="row">
                  <div className='l-4 m-4 c-5 detail__left'>
                    <div className="detail-main-picture">
                      <Image src={mainImage} alt="" />
                    </div>
                    <div className="detail-pictureList-container">
                      <div className="row detail-row-pictureList">
                        <div className="l-8 m-8 c-8">
                          <div className="row detail-pictureList">
                            {productItem?.photos.map((item, i) => (<DisplayMiniImage key={i} {...item} />))}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="l-1 m-1 c-0"></div>
                  <div className='l-7 m-7 c-7 detail-right'>
                    <div className='detail-name'>
                      <h1>{productItem?.productName}</h1>
                    </div>
                    <div className='detail-brand'>
                      <span>Brand:</span> <span className='detail-brand-info'>{productItem?.brand.name}</span>
                    </div>
                    <div className='detail-rated'>
                      <div className='detail-rated-text'>
                        <p>Status:</p>
                      </div>
                      <StatusMessage value={Number(productItem?.quantityInStock)} />
                    </div>
                    <div className='detail-guarantee'>
                      <span>Guarantee:</span>
                      <span className='detail-guarantee-text'>{productItem?.productGuarantee}</span>
                    </div>
                    <div className='detail-price'>
                      <b>${productItem?.sellPrice}</b>
                    </div>
                    <div className='detail-adjust-quantity'>
                      <button onClick={() => { quantity >= 2 ? setQuantity(quantity - 1) : setQuantity(1) }} className='detail-minus'>
                        -
                      </button>
                      <span className="detail-quantity">{quantity}</span>
                      <button onClick={() => setQuantity(quantity + 1)} className='detail-plus'>
                        +
                      </button>
                    </div>
                    <div className='detail-button'>
                      <button onClick={() => saveLocalStorage()} disabled={productItem?.quantityInStock === 0} className={`detail-button_btn ${productItem?.quantityInStock === 0 ? 'disabledButton' : ''}`}>
                        ADD TO CART
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={`detail-description-container `}>
        <div className={`grid wide ${style}`}>
          <div className="row">
            <div className="l-12 m-12 c-12 detail-description-title">
              <h1 style={{ fontSize: "1rem" }}>
                Description
              </h1>
              <div className='detail-description'>
                <p>
                  {productItem?.productDescription}
                </p>
                {
                  productItem?.photos?.[0]?.url ? (
                    <div className={`detail-description-image`}>
                      <Image className="full-size-image" src={productItem.photos[0].url} alt="" />
                    </div>
                  ) : null
                }
              </div>
            </div>
          </div>
        </div>
      </div >
      <div className='l-12 m-12 c-12 recss-detail-btn detail-button'>
        <button id="detail-btn-view" onClick={changeStyle}>{style === 'half-height' ? 'VIEW ALL' : 'VIEW LESS'}</button>
      </div>
      <div className='detail-review-container'>
        <div className='detail-review_wrap'>
          <div className='detail-review-header'>
            <h2>REVIEW</h2>
          </div>
          <div className="detail-review-body">
            <div className="detail-review-filter">
              <div className='detail-review-filter_wrap row'>
                <div className="m-3">
                  <div>
                    <span className='detail-review-filter_totalRate'>{averageRate}</span><span className='detail-review-filter_maxRate'> / 5</span>
                  </div>
                  <Rating value={Math.round(averageRate)} />
                </div>
                <div className="m-6 detail-review_filter-btn">
                  <button onClick={e => {
                    setRate('');
                    setStyleAllBtn('btnAll-active');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setHaveComment('');
                  }} className={`detail-review_filter-btn-all ${styleAllBtn}`}>All <span>({allRate})</span>
                  </button>
                  <button onClick={e => {
                    setRate('5');
                    setStyleAllBtn('');
                    setStyleFiveBtn('btnFive-active');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setHaveComment('');
                  }} className={`${styleFiveBtn}`}>5 stars <span>({fiveRate})</span></button>
                  <button onClick={e => {
                    setRate('4');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('btnFour-active');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setHaveComment('');
                  }} className={`${styleFourBtn}`}>4 stars <span>({fourRate})</span></button>
                  <button onClick={e => {
                    setRate('3');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('btnThree-active');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setHaveComment('');
                  }} className={`${styleThreeBtn}`}>3 stars <span>({threeRate})</span></button>
                  <button onClick={e => {
                    setRate('2');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('btnTwo-active');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setHaveComment('');

                  }} className={`${styleTwoBtn}`}>2 stars <span>({twoRate})</span></button>
                  <button onClick={e => {
                    setRate('1');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('btnOne-active');
                    setStyleCommentBtn('');
                    setHaveComment('');
                  }} className={`${styleOneBtn}`}>1 star <span>({oneRate})</span></button>
                  <button onClick={e => {
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('btnComment-active');
                    setHaveComment('1');
                  }} className={`${styleCommentBtn}`}>Have comment <span>({comment})</span></button>
                </div>
              </div>
              <div className='detail-review-list'>
                {reviewList.map((item, i) => (
                  <DisplayReview key={i} {...item} />
                ))}
              </div>
            </div>
          </div>
          <div className="pagination">
            <PreviousButton />
            <NumberPagination />
            <NextButton />
          </div>
        </div>
      </div >
      <div className='productList-container'>
        <div className="grid">
          <div className="row">
            <h1 className='l-12 m-12 c-12 detail-title'>Related Products</h1>
            <div className="productItem-container">
              <div className="row" id="productList-wrap">
                {
                  productList.map((item, i) => (<ProductItem key={i} product={item} />))
                }
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  );
}

export default Detail;

