import React, { useCallback, useEffect, useRef, useState } from 'react';
import { MdKeyboardDoubleArrowRight } from "react-icons/md";
import './Products.css';
import './Grid.css';
import './Cart.css';
import { Product } from './Slider';
import { OrderDetail } from './Products';
import Products from './Products';
import { Link, useNavigate } from 'react-router-dom';
import Image from './Image';



interface OrderItemProps {
  orderDetail: OrderDetail;
  removeItem: (id: number) => void;
  decreaseQuantity: (id: number) => void;
  increaseQuantity: (id: number) => void;
}

interface DisplayImageProps {
  imageName: string;
  imageURL: string;
}

const DisplayImage = (props: DisplayImageProps) => {
  const { imageName, imageURL } = props;

  return (
    <div className='l-3 c-3 m-3 cart__body-productInfo-img'>
      <Image className='cart__body-img' src={imageURL} alt={imageName} />
    </div>
  )
};

// Hàm hiển thị order
function OrderItem(props: OrderItemProps) {
  const { orderDetail, removeItem, increaseQuantity, decreaseQuantity } = props;
  const { product, quantity } = orderDetail;
  const { id, productName, color, sellPrice, photos } = product;
  const imageURL = photos[0]?.url;

  return (
    <div className='row cart__body-productItem'>
      <div className="l-6 m-6 c-6 cart__body-productInfo">
        <div className="row">
          <div className='l-3 c-3 m-3 cart__body-productInfo-img'>
            <Image className='cart__body-img' src={imageURL} alt={productName} />
          </div>
          <div className='l-9 c-9 m-9 cart__body-productName'>
            <b className=''>
              {productName}
            </b>
            <div className='cart__body-productColor'>
              <span>Color: </span>
              <span>{color.name}</span>
            </div>
            <div className='cart__body-removeBtn'>
              <button onClick={() => removeItem(id)}>
                Remove
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="l-2 m-2 c-2 cart__body-productPrice">
        <b>${sellPrice}</b>
      </div>
      <div className="l-2 m-2 c-2 cart__body-productQuantity">
        <button onClick={() => decreaseQuantity(id)} className='cart__body-buttonMinus'>-</button>
        <input className='car__body-inputQuantity' disabled value={quantity} type="text" />
        <button onClick={() => increaseQuantity(id)} className='cart__body-buttonPlus'>+</button>
      </div>
      <div className="l-2 m-2 c-2 cart__body-totalPrice">
        <span><b>$</b></span><b className="cart__body-totalPrice-number">{sellPrice * Number(quantity)}</b>
      </div>
    </div>
  );
}

function Cart() {
  const navigate = useNavigate();

  const [orders, setOrders] = useState<OrderDetail[]>([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [style, setStyle] = useState('')

  //Hàm lấy dữ liệu order từ localStorage
  useEffect(() => {
    function checkOrder() {
      const orders = JSON.parse(localStorage.getItem('orders') || '[]');
      setOrders(orders);
    }

    window.addEventListener('storage', checkOrder);

    checkOrder();
  }, []);

  //Tính tổng số tiền
  function TotalPrice() {
    let totalNumber = 0;
    orders.map((orderDetail) => (
      totalNumber = totalNumber + (orderDetail.quantity * orderDetail.sellPrice)
    ))

    return (
      <p className='totalPrice-number'>${totalNumber}</p>
    )
  }

  // Đến trang thanh toán
  function goToPaymentPage() {
    navigate('/payment')
  }


  // Hàm tăng số lượng sản phẩm
  const increaseQuantity = (id: number) => {
    const orderList = orders.map((orderDetail) => {
      if (orderDetail.product.id === id) {
        orderDetail.quantity += 1;
      }

      return orderDetail;
    });

    localStorage.setItem("orders", JSON.stringify(orderList));
    window.dispatchEvent(new Event("storage"));
  }

  // Hàm giảm số lượng sản phẩm
  const decreaseQuantity = (id: number) => {

    const orderList = orders.map((orderDetail) => {
      if (orderDetail.product.id === id && orderDetail.quantity >= 2) {
        orderDetail.quantity -= 1;
      } else if (orderDetail.product.id === id && orderDetail.quantity === 1) {
        removeItem(id);
      }

      return orderDetail;
    });

    localStorage.setItem("orders", JSON.stringify(orderList));
    window.dispatchEvent(new Event("storage"));
  }

  // Xóa item
  function removeItem(id: number) {
    const orderList = orders.filter((orderDetail) => orderDetail.product.id !== id)
    localStorage.setItem("orders", JSON.stringify(orderList));
    window.dispatchEvent(new Event("storage"));
  }

  return (
    <>
      <div className="cart__nav">
        <div className="row cart__nav-wrap">
          <Link to="/" className="cart_nav-home">
            <b>Home</b>
          </Link>
          <div className="detail-nav-icon">
            <MdKeyboardDoubleArrowRight />
          </div>
          <div className="cart_nav-all-product">
            <b>Cart</b>
          </div>
        </div>
      </div>

      <div className='cart__container'>
        <div className="cart__container-detail">
          <div className="cart__header">
            <div className="row ">
              <div className="l-6 m-6 c-6 cart__header-productInfo">
                <b>Product Info</b>
              </div>
              <div className="l-2 m-2 c-2 cart__header-eachPrice">
                <b>Each Price</b>
              </div>
              <div className="l-2 m-2 c-2 cart__header-quantity">
                <b>Quantity</b>
              </div>
              <div className="l-2 m-2 c-2 cart__header-total">
                <b>Total</b>
              </div>
            </div>
          </div>

          <div className="cart__body">
            {
              orders.map(item => (
                <OrderItem key={item.product.id} orderDetail={item} removeItem={removeItem} decreaseQuantity={decreaseQuantity} increaseQuantity={increaseQuantity} />
              ))
            }
          </div>
        </div>

        <div className="cart__bottom">
          <div className="row ">
            <div className="l-9 m-9 c-8"></div>
            <div className="l-3 m-3 c-4 cart__bottom-totalPrice">
              <p>Total:</p>
              <TotalPrice />
            </div>
          </div>
          <div className="row">
            <div className="l-9 m-9 c-9"></div>
            <div className="l-3 m-3 c-3 cart__bottom-paymentBtn">
              <button onClick={goToPaymentPage}>Payment</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Cart;

