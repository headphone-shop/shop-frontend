import React, { HTMLProps } from 'react';

const Image = (props: HTMLProps<HTMLImageElement>) => {
  const { src, alt, ...others } = props;

  return (
    <img src={src || '/shop/image-placeholder.webp'} alt={alt} {...others} />
  )
};

export default Image;

