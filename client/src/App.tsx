import React from "react";
import { useState } from "react";
import { Routes, Route, Outlet, Link } from "react-router-dom";
import Home from "./Home";
import Header from "./Header";
import Footer from "./Footer";
import Detail from "./Detail";
import Products from './Products';
import Cart from "./Cart";
import Login from "./Login";
import Register from "./Register";
import Payment from "./Payment";
import OrderStatus from "./OrderStatus";
import Profile from "./Profile";
import ChangePassword from "./ChangePassword";
import MyOrder from "./MyOrder";
import Review from "./Review";



export default function App() {
    return (
        <>
            {/* Routes nest inside one another. Nested route paths build upon
            parent route paths, and nested route elements render inside
            parent route elements. See the note about <Outlet> below. */}
            <Routes>
                <Route path="/" element={<Layout />}>
                    <Route index element={<Home />} />
                    <Route path="detail" element={<Detail />} />
                    <Route path="products" element={<Products />} />
                    <Route path="cart" element={<Cart />} />
                    <Route path="login" element={<Login />} />
                    <Route path="register" element={<Register />} />
                    <Route path="payment" element={<Payment />} />
                    <Route path="order-status" element={<OrderStatus />} />
                    <Route path="profile" element={<Profile />} />
                    <Route path="change-password" element={<ChangePassword />} />
                    <Route path="my-order" element={<MyOrder />} />
                    <Route path="review" element={<Review />} />

                    {/* Using path="*"" means "match anything", so this route
                acts like a catch-all for URLs that we don't have explicit
                routes for. */}
                    <Route path="*" element={<NoMatch />} />
                </Route>

                <Route path="*" element={<NoMatch />} />
            </Routes>
        </>
    );
}

function Layout() {
    return (
        <div>
            <Header />

            {/* An <Outlet> renders whatever child route is currently active,
          so you can think about this <Outlet> as a placeholder for
          the child routes we defined above. */}
            <Outlet />
            <Footer />

        </div>
    );
}


function Dashboard() {
    return (
        <div>
            <h2>Dashboard</h2>
        </div>
    );
}

function NoMatch() {
    return (
        <div>
            <h2>Nothing to see here!</h2>
            <p>
                <Link to="/">Go to the home page</Link>
            </p>
        </div>
    );
}