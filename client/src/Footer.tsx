import React from 'react';
import { FaYoutube, FaFacebook, FaTwitter } from "react-icons/fa6";
import './Footer.css';
import { AiFillInstagram } from "react-icons/ai";
function Footer() {
    return (
        <div className='footer'>
            <div className="grid">
                <div className="row">
                    <div className='l-3 m-6 c-6 footer__item'>
                        <p style={{ fontWeight: 700, margin: 0 }}>PRODUCTS</p>
                        <ul style={{ paddingLeft: "0" }} className='footer_list'>
                            <li>Help Center</li>
                            <li>Contact Us</li>
                            <li>Product Help</li>
                            <li>Warranty</li>
                            <li>Order Status</li>
                        </ul>
                    </div>
                    <div className='l-3 m-6 c-6 footer__item'>
                        <p style={{ fontWeight: 700, margin: 0 }}>SERVICES</p>
                        <ul style={{ paddingLeft: "0" }} className='footer_list'>
                            <li>Help Center</li>
                            <li>Contact Us</li>
                            <li>Product Help</li>
                            <li>Warranty</li>
                            <li>Order Status</li>
                        </ul>
                    </div>
                    <div className='l-3 m-6 c-6 footer__item'>
                        <p style={{ fontWeight: 700, margin: 0 }}>SUPPORT</p>
                        <ul style={{ paddingLeft: "0" }} className='footer_list'>
                            <li>Help Center</li>
                            <li>Contact Us</li>
                            <li>Product Help</li>
                            <li>Warranty</li>
                            <li>Order Status</li>
                        </ul>
                    </div>
                    <div className='l-3 m-6 c-6 footer__item'>
                        <h3>DEVCAMP</h3>
                        <ul style={{ paddingLeft: "0" }} className='footer_list--icon'>
                            <li><button><FaFacebook /></button></li>
                            <li><button><AiFillInstagram /></button></li>
                            <li><button><FaYoutube /></button></li>
                            <li><button><FaTwitter /></button></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Footer;