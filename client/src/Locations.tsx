import React, { ChangeEventHandler, useEffect, useState } from 'react';
import { Customer, District, Province, Street, Ward } from './ProductItem';
import './Payment.css';

export interface ProvinceProps {
    id: number;
    provinceName: string | undefined;
}
export interface DistrictProps {
    id: number;
    districtName: string | undefined;
}
export interface WardProps {
    id: number;
    wardName: string | undefined;
}
export interface StreetProps {
    id: number;
    streetName: string | undefined;
}

export interface SelectProps {
    message: string;
    id: string;
    name: string;
    className: string;
    placeholder: string;
    type: string;
    onChange: ChangeEventHandler<HTMLSelectElement>;
    value: string;
}
export interface LocationProps {
    receiverProvince: string;
    setReceiverProvince: React.Dispatch<React.SetStateAction<string>>;
    messageProvince: string;

    receiverDistrict: string;
    setReceiverDistrict: React.Dispatch<React.SetStateAction<string>>;
    messageDistrict: string;


    receiverWard: string;
    setReceiverWard: React.Dispatch<React.SetStateAction<string>>;
    messageWard: string;


    receiverStreet: string;
    setReceiverStreet: React.Dispatch<React.SetStateAction<string>>;
    messageStreet: string;

}

function Locations(props: LocationProps) {
    const { receiverProvince, setReceiverProvince, receiverDistrict, setReceiverDistrict, receiverWard, setReceiverWard, receiverStreet, setReceiverStreet, messageProvince, messageDistrict, messageWard, messageStreet } = props;

    const [provinces, setProvinces] = useState<Province[] | null>(null);
    const [districts, setDistricts] = useState<District[] | null>(null);
    const [wards, setWards] = useState<Ward[] | null>(null);
    const [streets, setStreets] = useState<Street[] | null>(null);

    // Hàm hiển thị select province;
    function SelectProvince(props: SelectProps) {
        const { message, id, name, className, placeholder, value, onChange } = props;
        return (
            <div className="l-6 m-12 c-12">
                <label htmlFor="select-province" style={{ fontSize: "0.8rem", marginLeft: "0.5rem", fontStyle: "italic" }}>Province</label>
                <select id="select-province" name={name} value={value}
                    className={`${className} ${message ? 'input-invalid' : ''}`}
                    onChange={onChange}>
                    <option value="" disabled={value ? true : false} defaultValue={''}>
                        {placeholder}
                    </option>
                    {provinces?.map(item => <ProvinceOption key={item.id} id={item.id} provinceName={item.provinceName} />)}
                </select>
                <div className={`invalid-message ${message ? '' : 'hidden'}`}>{message}</div>
            </div>
        )
    }

    // Hàm hiển thị select district
    function SelectDistrict(props: SelectProps) {
        const { message, id, name, className, placeholder, value, onChange } = props;
        return (
            <div className="l-6 m-12 c-12">
                <label htmlFor="select-district" style={{ fontSize: "0.8rem", marginLeft: "0.5rem", fontStyle: "italic" }}>District</label>
                <select id="select-district" disabled={!receiverProvince ? true : false} name={name} value={value}
                    className={`${className} ${message ? 'input-invalid' : ''}`}
                    onChange={onChange}>
                    <option value="" disabled defaultValue={''}>
                        {placeholder}
                    </option>
                    {districts?.map(item => <DistrictOption key={item.id} id={item.id} districtName={item.districtName} />)}
                </select>
                <div className={`invalid-message ${message ? '' : 'hidden'}`}>{message}</div>
            </div>
        )
    }

    // Hàm hiển thị select ward
    function SelectWard(props: SelectProps) {
        const { message, id, name, className, placeholder, value, onChange } = props;
        return (
            <div className="l-6 m-12 c-12">
                <label htmlFor="select-ward" style={{ fontSize: "0.8rem", marginLeft: "0.5rem", fontStyle: "italic" }}>Ward</label>
                <select id="select-ward" disabled={!receiverDistrict ? true : false} name={name} value={value}
                    className={`${className} ${message ? 'input-invalid' : ''}`}
                    onChange={onChange}>
                    <option value="" disabled defaultValue={''}>
                        {placeholder}
                    </option>
                    {wards?.map(item => <WardOption key={item.id} id={item.id} wardName={item.wardName} />)}
                </select>
                <div className={`invalid-message ${message ? '' : 'hidden'}`}>{message}</div>
            </div>
        )
    }

    // Hàm hiển thị select street
    function SelectStreet(props: SelectProps) {
        const { message, id, name, className, placeholder, value, onChange } = props;
        return (
            <div className="l-6 m-12 c-12">
                <label htmlFor="select-street" style={{ fontSize: "0.8rem", marginLeft: "0.5rem", fontStyle: "italic" }}>Street</label>
                <select id="select-street" disabled={!receiverWard ? true : false} name={name} value={value}
                    className={`${className} ${message ? 'input-invalid' : ''} select-street`}
                    onChange={onChange}>
                    <option value="" disabled defaultValue={''}>
                        {placeholder}
                    </option>
                    {streets?.map(item => <StreetOption key={item.id} id={item.id} streetName={item.streetName} />)}
                </select>
                <div className={`invalid-message ${message ? '' : 'hidden'}`}>{message}</div>
            </div>
        )
    }

    // Hàm gọi API lấy dữ liệu province
    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/provinces/`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((actualData) => {
                setProvinces(actualData);
            })
    }, []);

    //Gọi API lấy danh sách districts theo provinceID
    useEffect(() => {
        if (receiverProvince) {
            fetch(`${process.env.REACT_APP_API_URL}/provinces/${receiverProvince}/districts`)
                .then((response) => {
                    if (!response.ok) {
                        throw new Error(
                            `This is an HTTP error: The status is ${response.status}`
                        );
                    }
                    return response.json();
                })
                .then((actualData) => {
                    setDistricts(actualData);
                })
        }

    }, [receiverProvince]);

    //Gọi API lấy danh sách wards theo districtID
    useEffect(() => {
        if (receiverDistrict) {
            fetch(`${process.env.REACT_APP_API_URL}/districts/${receiverDistrict}/wards`)
                .then((response) => {
                    if (!response.ok) {
                        throw new Error(
                            `This is an HTTP error: The status is ${response.status}`
                        );
                    }
                    return response.json();
                })
                .then((actualData) => {
                    setWards(actualData);
                })
        }
    }, [receiverDistrict]);

    //Gọi API lấy danh sách streets theo districtID
    useEffect(() => {
        if (receiverDistrict) {
            fetch(`${process.env.REACT_APP_API_URL}/districts/${receiverDistrict}/streets`)
                .then((response) => {
                    if (!response.ok) {
                        throw new Error(
                            `This is an HTTP error: The status is ${response.status}`
                        );
                    }
                    return response.json();
                })
                .then((actualData) => {
                    setStreets(actualData);
                })
        }
    }, [receiverDistrict]);

    //Hàm load dữ liệu province vào select
    function ProvinceOption(props: ProvinceProps) {
        const { id, provinceName } = props;
        return <option key={id} value={id}>{provinceName}
        </option>;

    }

    //Hàm load dữ liệu district vào select
    function DistrictOption(props: DistrictProps) {
        const { id, districtName } = props;
        return <option key={id} value={id}>{districtName}
        </option>;

    }

    //Hàm load dữ liệu ward vào select
    function WardOption(props: WardProps) {
        const { id, wardName } = props;
        return <option key={id} value={id}>{wardName}
        </option>;

    }

    //Hàm load dữ liệu street vào select
    function StreetOption(props: StreetProps) {
        const { id, streetName } = props;
        return <option key={id} value={id}>{streetName}</option>;
    }

    return (
        <div className='payment_form-item'>

            <div className="row payment_form-province-district">
                <SelectProvince message={messageProvince} id={''} name={'province'} className={'payment_form-province'} placeholder={'-- Select province --'} type={''} onChange={(e) => {
                    setReceiverProvince(e.target.value);
                    setReceiverDistrict('');
                    setReceiverWard('');
                    setReceiverStreet('');
                }} value={receiverProvince} />
                <SelectDistrict message={messageDistrict} id={''} name={'district'} className={'payment_form-district'} placeholder={'-- Select district --'} type={''} onChange={e => {
                    setReceiverDistrict(e.target.value);
                    setReceiverWard('');
                    setReceiverStreet('');
                }} value={receiverDistrict} />
            </div>
            <div className="row payment_form-ward-street">
                <SelectWard message={messageWard} id={''} name={'ward'} className={'payment_form-ward'} placeholder={'-- Select ward --'} type={''} onChange={e => {
                    setReceiverWard(e.target.value);
                }} value={receiverWard} />
                <SelectStreet message={messageStreet} id={''} name={'street'} className={'payment_form-ward'} placeholder={'-- Select street --'} type={''} onChange={e => setReceiverStreet(e.target.value)} value={receiverStreet} />
            </div>
        </div>
    )
}

export default Locations;
