import React, { useEffect, useState } from 'react';
import './App.css';
import './Grid.css';
import './ProductItem.css';
import './ProductItem.css';
import ProductItem from './ProductItem';
import Slider, { Product, ProductLine } from './Slider';


function Home() {
  const [productList, setProductList] = useState<Product[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [pageNum, setPageNum] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [style, setStyle] = useState('');

  function nextPage() {
    setPageNum(pageNum + 1)
  }

  useEffect(() => {
    const searchParams = new URLSearchParams();

    searchParams.append('page', String(pageNum));
    searchParams.append('size', String(4));

    fetch(`${process.env.REACT_APP_API_URL}/product/paging1?${searchParams}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setProductList(productList => productList.concat(actualData.content))
        setTotalPages(actualData.totalPages);
      });

  }, [pageNum]);

  return (
    <>
      <Slider productList={productList} />
      <div className='productList-container'>
        <div className="grid">
          <div className="row">
            <h1 className='l-12 m-12 c-12 productList-title'>LATEST PRODUCT</h1>
            <div className='productItem-container'>
              <div className="row" id="productList-wrap">
                {
                  productList.map((item) => (<ProductItem key={item.id} product={item} />))
                }
              </div>
            </div>
            <div className='l-12 m-12 c-12 productList-btn'>
              <button className={(pageNum >= totalPages - 1) ? 'displayHide' : ''} disabled={pageNum >= totalPages - 1} onClick={nextPage}>VIEW MORE</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;

